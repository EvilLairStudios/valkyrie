package io.evillair.mqtt

import akka.stream.scaladsl.Flow
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.util.{ByteIterator, ByteString}

import scala.util.{Failure, Success, Try}

object MQTTFraming {

  /**
    * @param lengthFieldOffset  The offset of the field from the beginning of the frame in bytes
    * @param maximumFrameLength The maximum length of allowed frames while decoding. If the maximum length is exceeded
    *                           this Flow will fail the stream. This length *includes* the header (i.e the offset and
    *                           the length of the size field)
    */
  def variableLengthField(lengthFieldOffset: Int, maximumFrameLength: Int) = {
    Flow[ByteString].via(new VariableLengthFieldFramingStage(lengthFieldOffset, maximumFrameLength))
        .named("variableLengthFieldFraming")
  }

  class MQTTFramingException(msg: String) extends RuntimeException(msg)

  private final val variableByteIntegerDecoder: (ByteIterator, Int) => Int = (bs, length) => {
    val maxMultiplier: Double = math.pow(128, length-1)
    def loop(multiplier: Int, value: Int): Int = {
      if (multiplier > maxMultiplier) throw new MQTTFramingException("Too many iterations decoding length frame")
      val nextInt: Int = bs.next().toInt & 0xff
      val encoded: Int = (nextInt & 0x7f) * multiplier
      nextInt & 0x80 match {
        case 0 => if (multiplier != maxMultiplier) throw new MQTTFramingException("Too few iterations decoding length frame") else value + encoded
        case 128 => loop(multiplier*128, value + encoded)
      }
    }
    loop(1,0)
  }


  private final class VariableLengthFieldFramingStage(
                                                       val lengthFieldOffset:  Int,
                                                       val maximumFrameLength: Int
                                                     ) extends GraphStage[FlowShape[ByteString, ByteString]] {

    private val startingChunkSize = lengthFieldOffset + 1
    private val maxLengthFieldLength = 4
    private val maximumChunkSize = lengthFieldOffset + maxLengthFieldLength
    private val intDecoder = variableByteIntegerDecoder

    val in = Inlet[ByteString]("VariableLengthFieldFramingStage.in")
    val out = Outlet[ByteString]("VariableLengthFieldFramingStage.out")
    override val shape: FlowShape[ByteString, ByteString] = FlowShape(in, out)

    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) with InHandler with OutHandler {
      private var buffer = ByteString.empty
      private var frameSize = Int.MaxValue
      private var minimumChunkSize = startingChunkSize

      private def searchLength(maxBytes: Int): Int = {
        buffer.iterator
          .drop(lengthFieldOffset)
          .take(maxBytes).toVector
          .map(_.toInt & 0xff)
          .map(_ & 0x80)
          .indexWhere(_ == 0)
      }

      /**
        * push, and reset frameSize and buffer
        *
        */
      private def pushFrame() = {
        val emit = buffer.take(frameSize).compact
        buffer = buffer.drop(frameSize)
        frameSize = Int.MaxValue
        minimumChunkSize = startingChunkSize // If we've pushed a frame, reset this for the next
        push(out, emit)
        if (buffer.isEmpty && isClosed(in)) {
          completeStage()
        }
      }

      /**
        * try to push downstream, if failed then try to pull upstream
        *
        */
      private def tryPushFrame() = {
        val buffSize = buffer.size
        if (buffSize >= frameSize) {
          pushFrame()
        } else if (buffSize >= minimumChunkSize) {

          val lengthBytesNeeded: Int = buffSize match {
            case a if a >= maximumChunkSize => searchLength(4)
            case b if b >= maximumChunkSize - 1 => searchLength(3)
            case c if c >= maximumChunkSize - 2 => searchLength(2)
            case _ => searchLength(1)
          }

          lengthBytesNeeded match {
            case -1 => {
              minimumChunkSize = minimumChunkSize + 1
              if (minimumChunkSize > maximumChunkSize) {
                failStage(new MQTTFramingException("Unable to parse length from frame header in under 4 bytes"))
              } else tryPull()
            }
            case n => {
              val lbn: Int = n + 1
              Try(intDecoder(buffer.iterator.drop(lengthFieldOffset), lbn)) match {
                case Failure(exception) => {
                  failStage(exception)
                }
                case Success(parsedLength) => {
                  frameSize = parsedLength + lengthFieldOffset + lbn
                  if (frameSize > maximumFrameLength) {
                    failStage(new MQTTFramingException(s"Maximum allowed frame size is $maximumFrameLength but decoded frame header reported size $frameSize"))
                  } else if (parsedLength < 0) {
                    failStage(new MQTTFramingException(s"Decoded frame header reported negative size $parsedLength"))
                  } else if (frameSize < minimumChunkSize) {
                    failStage(new MQTTFramingException(s"Computed frame size $frameSize is less than minimum chunk size $minimumChunkSize"))
                  } else if (buffSize >= frameSize) {
                    pushFrame()
                  } else tryPull()
                }
              }
            }
          }

        } else tryPull()
      }

      private def tryPull() = {
        if (isClosed(in)) {
          failStage(new MQTTFramingException("Stream finished but there was a truncated final frame in the buffer"))
        } else pull(in)
      }

      override def onPush(): Unit = {
        buffer ++= grab(in)
        tryPushFrame()
      }

      override def onPull() = tryPushFrame()

      override def onUpstreamFinish(): Unit = {
        if (buffer.isEmpty) {
          completeStage()
        } else if (isAvailable(out)) {
          tryPushFrame()
        } // else swallow the termination and wait for pull
      }

      setHandlers(in, out, this)
    }
  }


}
