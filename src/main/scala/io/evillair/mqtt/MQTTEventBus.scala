package io.evillair.mqtt

import akka.actor.ActorRef
import akka.event.{EventBus, LookupClassification}
import akka.util.ByteString



final case class MQTTEnvelope(topic: String, payload: ByteString)

/**
  * A topic-based EventBus for MQTT pub-sub
  */
class MQTTEventBus extends EventBus with LookupClassification {

  type Event = MQTTEnvelope
  type Classifier = String
  type Subscriber = ActorRef

  override protected def classify(event: Event): Classifier = event.topic

  override protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event.payload
  }

  override protected def compareSubscribers(a: Subscriber, b: Subscriber): Int =
    a.compareTo(b)

  override protected def mapSize: Int = 128


}

object MQTTEventBus {
  lazy val eventBus = new MQTTEventBus
}
