package io.evillair.mqtt.models

/**
  * Connect Reason Codes
  * [[http://docs.oasis-open.org/mqtt/mqtt/v5.0/cs02/mqtt-v5.0-cs02.html#_Toc514345351 3.2.2.2]]
  *
  * If a Server sends a CONNACK packet containing a Reason code of 128 or greater it MUST then
  * close the Network Connection [MQTT-3.2.2-7]
  */
object ReasonCode {

  /**
    * Success
    *
    * CONNACK, PUBACK, PUBREC, PUBREL, PUBCOMP, UNSUBACK, AUTH
    */
  val SUCCESS: Int = 0x00

  /**
    * Normal disconnection
    *
    * DISCONNECT
    */
  val NORMAL_DISCONNECT: Int = 0x00

  /**
    * Granted QoS 0
    *
    * SUBACK
    */
  val GRANTED_QOS_0: Int = 0x00

  /**
    * Granted QoS 1
    *
    * SUBACK
    */
  val GRANTED_QOS_1: Int = 0x01

  /**
    * Granted QoS 2
    *
    * SUBACK
    */
  val GRANTED_QOS_2: Int = 0x02

  /**
    * Disconnect with Will Message
    *
    * DISCONNECT
    */
  val DISCONNECT_WITH_WILL: Int = 0x04

  /**
    * No matching subscribers
    *
    * PUBACK, PUBREC
    */
  val NO_MATCHING_SUBSCRIBERS: Int = 0x10

  /**
    * No subscription existed
    *
    * UNSUBACK
    */
  val NO_SUBSCRIPTIONS_EXISTED: Int = 0x11

  /**
    * Continue authentication
    *
    * AUTH
    */
  val CONTINUE_AUTH: Int = 0x18

  /**
    * Re-authenticate
    *
    * AUTH
    */
  val RE_AUTH: Int = 0x19

  /**
    * The Server does not wish to reveal the reason for the failure, or none of the other Reason Codes apply.
    *
    * CONNACK, PUBACK, PUBREC, SUBACK, UNSUBACK, DISCONNECT
    */
  val UNSPECIFIED_ERROR = 0x80

  /**
    * Malformed packet
    *
    * CONNACK, DISCONNECT
    */
  val MALFORMED_PACKET = 0x81

  /**
    * Protocol error
    *
    * CONNACK, DISCONNECT
    */
  val PROTOCOL_ERROR = 0x82

  /**
    * The packet is valid but is not accepted/implemented by this Server.
    *
    * CONNACK, PUBACK, PUBREC, SUBACK, UNSUBACK, DISCONNECT
    */
  val IMPLEMENTATION_SPECIFIC_ERROR = 0x83

  /**
    * The Server does not support the version of the MQTT protocol requested by the Client.
    *
    * CONNACK
    */
  val UNSUPPORTED_PROTOCOL_VERSION = 0x84

  /**
    * The Client Identifier is a valid string but is not allowed by the Server.
    *
    * CONNACK
    */
  val CLIENT_IDENTIFIER_NOT_VALID = 0x85

  /**
    * The Server does not accept the User Name or Password specified by the Client
    *
    * CONNACK
    */
  val BAD_USER_PASS = 0x86

  /**
    * The Client is not authorized
    *
    * CONNACK, PUBACK, PUBREC, SUBACK, UNSUBACK, DISCONNECT
    */
  val NOT_AUTHORIZED = 0x87

  /**
    * The MQTT Server is not available.
    *
    * CONNACK
    */
  val SERVER_UNAVAILABLE = 0x88

  /**
    * The Server is busy. Try again later.
    *
    * CONNACK, DISCONNECT
    */
  val SERVER_BUSY = 0x89

  /**
    * This Client has been banned by administrative action. Contact the server administrator.
    *
    * CONNACK
    */
  val CLIENT_BANNED = 0x8a

  /**
    * The server is shutting down
    *
    * DISCONNECT
    */
  val SERVER_SHUTTING_DOWN = 0x8b

  /**
    * The authentication method is not supported or does not match the authentication method currently in use.
    *
    * CONNACK, DISCONNECT
    */
  val BAD_AUTH_METHOD = 0x8c

  /**
    * Keep Alive timeout
    *
    * DISCONNECT
    */
  val KEEPALIVE_TIMEOUT: Int = 0x8d

  /**
    * Session has been taken over
    *
    * DISCONNECT
    */
  val SESSION_TAKEOVER: Int = 0x8e

  /**
    * The topic filter is invalid
    *
    * SUBACK, UNSUBACK, DISCONNECT
    */
  val TOPIC_FILTER_INVALID: Int = 0x8f

  /**
    * The topic name is not accepted by this Server.
    *
    * CONNACK, PUBACK, PUBREC, DISCONNECT
    */
  val TOPIC_NAME_INVALID: Int = 0x90

  /**
    * Packet identifier is in use.
    *
    * The response to this is either to try to fix the state, or to reset the Session state by connecting using
    * Clean Start set to 1, or to decide if the Client or Server implementations are defective.
    *
    * PUBACK, PUBREC, SUBACK, UNSUBACK
    */
  val PACKET_ID_IN_USE: Int = 0x91

  /**
    * Packet identifier was not found
    *
    * PUBREL, PUBCOMP
    */
  val PACKET_ID_NOT_FOUND: Int = 0x92

  /**
    * Receive maximum was exceeded
    *
    * DISCONNECT
    */
  val RECEIVE_MAX_EXCEEDED: Int = 0x93

  /**
    * Topic alias is invalid
    *
    * DISCONNECT
    */
  val TOPIC_ALIAS_INVALID: Int = 0x94

  /**
    * The packet exceeded the maximum permissible size.
    *
    * CONNACK, DISCONNECT
    */
  val PACKET_TOO_LARGE = 0x95

  /**
    * The maximum message rate was exceeded
    *
    * DISCONNECT
    */
  val MESSAGE_RATE_EXCEEDED: Int = 0x96

  /**
    * An implementation or administrative imposed limit has been exceeded.
    *
    * CONNACK, PUBACK, PUBREC, SUBACK, DISCONNECT
    */
  val QUOTA_EXCEEDED = 0x97

  /**
    * Administrative action was taken
    *
    * DISCONNECT
    */
  val ADMIN_ACTION: Int = 0x98

  /**
    * The payload does not match the specified Payload Format Indicator.
    *
    * CONNACK, PUBACK, PUBREC, DISCONNECT
    */
  val PAYLOAD_INVALID = 0x99

  /**
    * The Server does not support retained messages, and Will Retain was set to 1.
    *
    * CONNACK, DISCONNECT
    */
  val RETAIN_NOT_SUPPORTED = 0x9a

  /**
    * The Server does not support the QoS set in Will QoS.
    *
    * CONNACK, DISCONNECT
    */
  val QOS_NOT_SUPPORTED = 0x9b

  /**
    * The Client should temporarily use another server.
    *
    * CONNACK, DISCONNECT
    */
  val USE_ANOTHER_SERVER = 0x9c

  /**
    * The Client should permanently use another server.
    *
    * CONNACK, DISCONNECT
    */
  val SERVER_MOVED = 0x9d

  /**
    * Shared Subscriptions not supported
    *
    * SUBACK, DISCONNECT
    */
  val SHARED_SUBSCRIPTION_NOT_SUPPORTED: Int = 0x9e

  /**
    * The connection rate limit has been exceeded.
    *
    * CONNACK, DISCONNECT
    */
  val CONNECTION_RATE_EXCEEDED = 0x9f

  /**
    * Maximum allowed time to be connected ahs been exceeded
    *
    * DISCONNECT
    */
  val MAX_CONNECT_TIME_EXCEEDED: Int = 0xa0

  /**
    * Subscription Identifiers not supported
    *
    * SUBACK, DISCONNECT
    */
  val SUBSCRIPTION_IDENTIFIERS_NOT_SUPPORTED: Int = 0xa1

  /**
    * Wildcard Subscriptions not supported
    *
    * SUBACK, DISCONNECT
    */
  val SUBSCRIPTION_WILDCARDS_NOT_SUPPORTED: Int = 0xa2

}