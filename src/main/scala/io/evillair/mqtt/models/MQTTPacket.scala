package io.evillair.mqtt.models

import akka.util.ByteString
import io.evillair.mqtt.MQTTFraming.MQTTFramingException

trait MQTTPacket {

  def encodeLength(bString: ByteString): ByteString = {

    val length = bString.length

    def loop(x: Int, bs: ByteString): ByteString = x match {
      case _ if x < 0 => throw new MQTTFramingException(s"Error encoding length!")
      case _ if x == 0 => if (bs.nonEmpty) bs else ByteString(0x00)
      case _ => {
        val encoded = x % 128
        val nx = x / 128
        val toAppend = if (nx > 0) {
          ByteString(encoded | 128)
        } else {
          ByteString(encoded)
        }
        loop(nx, bs ++ toAppend)
      }
    }

    (loop(length, ByteString.empty) ++ bString).compact
  }

}

case class PublishPacket(topic: String, content: ByteString) extends MQTTPacket

case class SubscribePacket(topic: String) extends MQTTPacket
case class UnsubscribePacket(topic: String) extends MQTTPacket

case object ForceDisconnect extends MQTTPacket

