package io.evillair.mqtt.models
import akka.util.ByteString

case object PingResp extends ResponsePacket {
  override val payload: ByteString = ByteString(ControlPacketType.PINGRESP << 4, 0x00)
}
