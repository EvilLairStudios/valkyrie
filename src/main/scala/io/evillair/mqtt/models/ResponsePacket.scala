package io.evillair.mqtt.models

import akka.util.ByteString

/**
  * A ResponsePacket contains is a message intended to be written [server] -> [client]
  */
trait ResponsePacket extends MQTTPacket {
  val payload: ByteString
}
