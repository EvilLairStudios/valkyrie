package io.evillair.mqtt.models
import akka.util.ByteString

/**
  * Send a Disconnect to the client
  *
  * The Session Expiry Interval MUST NOT be sent on a DISCONNECT by the Server [MQTT-3.14.2-2].
  *
  * @param reasonCode
  * @param reasonString Human readable, designed for diagnostics and SHOULD NOT be parsed by the receiver.
  * @param serverReference Alternate server to use. Used with Reason Code 0x9C (Use another server) or 0x9D (Server moved)
  */
case class Disconnect(
                       reasonCode: Int,
                       reasonString: Option[String] = None,
                       serverReference: Option[String] = None
                     ) extends ResponsePacket {

  // TODO visit User Properties later. JSON encode?

  private val bsReason: ByteString = reasonString match {
    case Some(rsn) => ByteString(0x1f) ++ ByteString(rsn)
    case None => ByteString.empty
  }

  private val srvRef: ByteString = serverReference match {
    case Some(srv) => ByteString(0x1c) ++ ByteString(srv)
    case None => ByteString.empty
  }

  override val payload: ByteString = ByteString(
    ControlPacketType.DISCONNECT << 4) ++
    encodeLength(
      ByteString(reasonCode) ++ bsReason ++ srvRef
    )
}
