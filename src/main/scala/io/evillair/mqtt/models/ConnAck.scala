package io.evillair.mqtt.models
import akka.util.ByteString

/**
  * 3.2.2 CONNACK Variable Header
  *
  * The Variable Header of the CONNACK Packet contains the following fields in the order:
  * Connect Acknowledge Flags,
  * Connect Reason Code,
  * and Properties.
  * The rules for encoding Properties are described in section [[http://docs.oasis-open.org/mqtt/mqtt/v5.0/cs02/mqtt-v5.0-cs02.html#_Properties 2.2.2]].
  *
  */
case class ConnAck(reasonCode: Int, sessionPresent: Boolean) extends ResponsePacket {
  // TODO properties...
  val ack: Int = if (sessionPresent) 0x01 else 0x00
  override val payload: ByteString = ByteString(ControlPacket.CONNACK, 0x02, ack, reasonCode)
}
