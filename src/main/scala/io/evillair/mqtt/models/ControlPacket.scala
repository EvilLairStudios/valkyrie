package io.evillair.mqtt.models

import akka.util.ByteString


object ControlPacket {

  implicit class IntBitShifter(int: Int) {
    def bitShifted: Int = (int & 0x0f) << 4
  }

  /**
    * Reserved / Forbidden
    */
  val RESERVED_0: Int = 0.bitShifted

  /**
    * Client request to connect to Server
    * [client] -> [server]
    */
  val CONNECT: Int = 1.bitShifted

  /**
    * Connect acknowledgment
    * [server] -> [client]
    */
  val CONNACK: Int = 2.bitShifted

  /**
    * Publish message
    * [client] <-> [server]
    */
  val PUBLISH: Int = 3.bitShifted + 0x00 // TODO DUP, QOS, RETAIN

  /**
    * Publish acknowledgment
    * [client] <-> [server]
    */
  val PUBACK: Int = 4.bitShifted

  /**
    * Publish received (assured delivery part 1)
    * [client] <-> [server]
    */
  val PUBREC: Int = 5.bitShifted

  /**
    * Publish release (assured delivery part 2)
    * [client] <-> [server]
    */
  val PUBREL: Int = 6.bitShifted + 0x02

  /**
    * Publish complete (assured delivery part 3)
    * [client] <-> [server]
    */
  val PUBCOMP: Int = 7.bitShifted

  /**
    * Client subscribe request
    * [client] -> [server]
    */
  val SUBSCRIBE: Int = 8.bitShifted + 0x02

  /**
    * Subscribe acknowledgment
    * [server] -> [client]
    */
  val SUBACK: Int = 9.bitShifted

  /**
    * Unsubscribe request
    * [client] -> [server]
    */
  val UNSUBSCRIBE: Int = 10.bitShifted + 0x02

  /**
    * Unsubscribe acknowledgment
    * [server] -> [client]
    */
  val UNSUBACK: Int = 11.bitShifted

  /**
    * PING request
    * [client] -> [server]
    */
  val PINGREQ: Int = 12.bitShifted

  /**
    * PING response
    * [server] -> [client]
    */
  val PINGRESP: Int = 13.bitShifted

  /**
    * Client is disconnecting
    * [client] -> [server]
    */
  val DISCONNECT: Int = 14.bitShifted

  /** TODO this is AUTH in 5.0!
    * Reserved / Forbidden
    */
  val RESERVED_15: Int = 15.bitShifted
}

case class ControlPacket(header: Byte, content: ByteString) extends MQTTPacket

