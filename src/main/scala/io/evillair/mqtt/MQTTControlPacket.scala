package io.evillair.mqtt

import akka.util.ByteString

case class MQTTControlPacket(headerByte: Byte, content: ByteString) {

  val packetType: Int = headerByte & 0xF0 >> 4
  val packetFlags: Int = headerByte & 0x0F
}

object MQTTControlPacket {

  /**
    * Reserved / Forbidden
    */
  val RESERVED_0: Int = 0

  /**
    * Client request to connect to Server
    * [client] -> [server]
    */
  val CONNECT: Int = 1

  /**
    * Connect acknowledgment
    * [server] -> [client]
    */
  val CONNACK: Int = 2

  /**
    * Publish message
    * [client] <-> [server]
    */
  val PUBLISH: Int = 3

  /**
    * Publish acknowledgment
    * [client] <-> [server]
    */
  val PUBACK: Int = 4

  /**
    * Publish received (assured delivery part 1)
    * [client] <-> [server]
    */
  val PUBREC: Int = 5

  /**
    * Publish release (assured delivery part 2)
    * [client] <-> [server]
    */
  val PUBREL: Int = 6

  /**
    * Publish complete (assured delivery part 3)
    * [client] <-> [server]
    */
  val PUBCOMP: Int = 7

  /**
    * Client subscribe request
    * [client] -> [server]
    */
  val SUBSCRIBE: Int = 8

  /**
    * Subscribe acknowledgment
    * [server] -> [client]
    */
  val SUBACK: Int = 9

  /**
    * Unsubscribe request
    * [client] -> [server]
    */
  val UNSUBSCRIBE: Int = 10

  /**
    * Unsubscribe acknowledgment
    * [server] -> [client]
    */
  val UNSUBACK: Int = 11

  /**
    * PING request
    * [client] -> [server]
    */
  val PINGREQ: Int = 12

  /**
    * PING response
    * [server] -> [client]
    */
  val PINGRESP: Int = 13

  /**
    * Client is disconnecting
    * [client] -> [server]
    */
  val DISCONNECT: Int = 14

  /**
    * Reserved / Forbidden
    */
  val RESERVED_15: Int = 15


}
