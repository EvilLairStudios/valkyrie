package io.evillair.mqtt.actors

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.Tcp.{Bind, Bound, Connected, Register}
import akka.io.{IO, Tcp}

/**
  * An actor that acts as a TCP server, handing over new connections to
  * be handled by a TCPConnectionActor
  * @param address The address to bind to
  * @param port The port to bind to
  */
class TCPServerActor(address: String, port: Int) extends Actor {

  import context.system
  IO(Tcp) ! Bind(self, new InetSocketAddress(address, port))

  override def receive: Receive = {
    case Bound(local) => {
      println(s"Server started on $local")
    }
    case Connected(remote, local) => {
      val remoteConnection: ActorRef = sender()
      val handler = context.actorOf(TCPConnectionActor.props(remoteConnection, remote))
      println(s"New connection: $local -> $remote")
      sender() ! Register(handler)
    }

  }

}

object TCPServerActor {
  def props(address: String, port: Int) = Props(new TCPServerActor(address, port))
}