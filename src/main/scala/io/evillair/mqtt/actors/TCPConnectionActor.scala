package io.evillair.mqtt.actors

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.Tcp.{Close, ConnectionClosed, Received, Write}
import io.evillair.mqtt.models._

object TCPConnectionActor {
  def props(remoteConnection: ActorRef, remoteAddress: InetSocketAddress) =
    Props(new TCPConnectionActor(remoteConnection, remoteAddress))
}

/**
  * Handle an MQTT client connection
  * @param remoteConnection
  * @param remoteAddress
  */
class TCPConnectionActor(remoteConnection: ActorRef, remoteAddress: InetSocketAddress) extends Actor {


  /**
    * This actor buffers incoming data, and frames MQTT Control packets
    */
  val framingActor: ActorRef = context.actorOf(Props[FramingActor])

  /**
    * This actor processes framed MQTT Control packets
    */
  val controlActor: ActorRef = context.actorOf(Props[ControlActor])

  /**
    * This actor handles publishing of messages to topics
    */
  val publisherActor: ActorRef = context.actorOf(Props[PublisherActor])

  /**
    * This actor handles subscribing to topics
    */
  val subscriptionActor: ActorRef = context.actorOf(Props[SubscriptionActor])


  override def receive: Actor.Receive = {

    case Received(data) => framingActor ! data
    case cp: ControlPacket => controlActor ! cp

    case disconnect: Disconnect => {
      remoteConnection ! Write(disconnect.payload)
      remoteConnection ! Close
    }

    case rp: ResponsePacket => remoteConnection ! Write(rp.payload)

    case sub: SubscribePacket => subscriptionActor ! sub
    case unsub: UnsubscribePacket => subscriptionActor ! unsub
    case pub: PublishPacket => publisherActor ! pub


    case message: ConnectionClosed =>
      println("Connection has been closed")
      context stop self
  }
}
