package io.evillair.mqtt.actors

import akka.actor.Actor
import io.evillair.mqtt.{MQTTEnvelope, MQTTEventBus}
import io.evillair.mqtt.models.PublishPacket

/**
  * Publish MQTT payloads to topics
  */
class PublisherActor extends Actor {

  override def receive: Receive = {
    case pub: PublishPacket => MQTTEventBus.eventBus.publish(MQTTEnvelope(pub.topic, pub.content))
  }

}
