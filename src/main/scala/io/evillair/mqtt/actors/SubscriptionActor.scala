package io.evillair.mqtt.actors

import akka.actor.Actor
import akka.util.ByteString
import io.evillair.mqtt.MQTTEventBus
import io.evillair.mqtt.models.{ResponsePacket, SubscribePacket, UnsubscribePacket}

/**
  * Handle MQTT subscriptions
  */
class SubscriptionActor extends Actor {

  // TODO handle alias'

  override def receive: Receive = {

    case sub: SubscribePacket => {
      MQTTEventBus.eventBus.subscribe(self, sub.topic)
    }

    case unsub: UnsubscribePacket => {
      MQTTEventBus.eventBus.unsubscribe(self, unsub.topic)
    }

    //case bs: ByteString => context.parent ! ResponsePacket(bs)

  }

}
