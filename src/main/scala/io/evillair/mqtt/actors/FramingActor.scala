package io.evillair.mqtt.actors

import akka.actor.{Actor, ActorLogging}
import akka.util.{ByteIterator, ByteString}
import io.evillair.mqtt.models.ControlPacket

class FramingActor extends Actor with ActorLogging {

  private var buffer: ByteString = ByteString.empty

  private val headerOffset: Int = 1
  private val maxBytesLength: Int = 4
  private val maxFrameLength = 268435455+5

  final case object Frame

  // TODO have parent watch for this exception, and close TCP connection!
  final class MQTTFramingException(msg: String) extends RuntimeException(msg)


  private final val variableByteIntegerDecoder: (ByteIterator, Int) => Int = (bs, length) => {
    val maxMultiplier: Double = math.pow(128, length-1)
    def loop(multiplier: Int, value: Int): Int = {
      if (multiplier > maxMultiplier) throw new MQTTFramingException("Too many iterations decoding length frame")
      val nextInt: Int = bs.next().toInt & 0xff
      val encoded: Int = (nextInt & 0x7f) * multiplier
      nextInt & 0x80 match {
        case 0 => if (multiplier != maxMultiplier) throw new MQTTFramingException("Too few iterations decoding length frame") else value + encoded
        case 128 => loop(multiplier*128, value + encoded)
      }
    }
    loop(1,0)
  }

  override def receive: Receive = {

    /**
      * Whenever we receive a ByteString, append it to our buffer
      * and attempt to Frame it
      */
    case bs: ByteString => {
      buffer ++= bs
      self ! Frame
    }

    /**
      * Attempt to Frame an MQTT Control packet
      */
    case Frame => {

      val buffSize: Int = buffer.size

      /**
        * An MQTT control packet can have up to 4 bytes that determine the length.
        * The final length byte should == 0, indicating the number of bytes needed.
        */
      val lengthBytesNeeded: Int = buffer.iterator
        .drop(headerOffset)
        .take(maxBytesLength)
        .map(_.toInt & 0xff)
        .map(_ & 0x80)
        .indexWhere(_ == 0) + 1

      lengthBytesNeeded match {

        /**
          * This indicates we either
          * A: don't have enough in our buffer to parse the length
          * B: we've received bad packets, and should terminate the TCP connection!
          */
        case 0 => {
          if (buffSize >= headerOffset + maxBytesLength) throw new MQTTFramingException("Error parsing MQTT Control packet")
        }

        /**
          * We've successfully calculated how many bytes we need to determine the remaining length
          */
        case len => {

          val parsedLength: Int = variableByteIntegerDecoder(buffer.iterator.drop(headerOffset), len)
          if (parsedLength < 0) throw new MQTTFramingException("Length too small!")

          val frameLength: Int = parsedLength + headerOffset + len
          if (frameLength > maxFrameLength) throw new MQTTFramingException("Frame too big!")

          /**
            * If we have enough data in our buffer, Frame it!
            */
          if (buffSize >= frameLength) {
            val emit: ByteString = buffer.take(frameLength).compact
            val controlPacket: ControlPacket = new ControlPacket(emit.head, emit.drop(headerOffset + len).compact)
            buffer = buffer.drop(frameLength)
            context.parent ! controlPacket
            /**
              * If we still have remaining data in out buffer, try to Frame again
              */
            if (buffer.nonEmpty) self ! Frame
          }
        }
      }


    }

  }

}
