package io.evillair.mqtt.actors

import akka.actor.{Actor, ActorLogging}
import io.evillair.mqtt.models._

/**
  * Parse framed MQTT control packets. These should catch things of direction
  * [client] -> [server]
  */
class ControlActor extends Actor with ActorLogging {
  override def receive: Receive = {

    case ctrl: ControlPacket => (ctrl.header & 0xf0) >> 4 match {

      case ControlPacketType.CONNECT => {
        log.info(s"${ctrl.header}: ${ctrl.content}")
        if ((ctrl.header & 0x0f) != 0) {
          context.parent ! Disconnect(ReasonCode.MALFORMED_PACKET)
        } else {
          context.parent ! ConnAck(ReasonCode.SUCCESS, sessionPresent = false)
        }
      }
      case ControlPacketType.PUBLISH => {
        ???
      }
      case ControlPacketType.PUBACK => {
        ???
      }
      case ControlPacketType.PUBREC => {
        ???
      }
      case ControlPacketType.PUBREL => {
        ???
      }
      case ControlPacketType.PUBCOMP => {
        ???
      }
      case ControlPacketType.SUBSCRIBE => {
        ???
      }
      case ControlPacketType.UNSUBSCRIBE => {
        ???
      }
      case ControlPacketType.PINGREQ => {
        if ((ctrl.header & 0x0f) == 0 && ctrl.content.isEmpty) {
          context.parent ! PingResp
        } else {
          context.parent ! Disconnect(ReasonCode.MALFORMED_PACKET)
        }
      }
      case ControlPacketType.DISCONNECT => {
        context.parent ! Disconnect(ReasonCode.NORMAL_DISCONNECT)
      }
      case ControlPacketType.RESERVED_0 => {
        context.parent ! Disconnect(ReasonCode.PROTOCOL_ERROR)
      }
      case ControlPacketType.RESERVED_15 => {
        context.parent ! Disconnect(ReasonCode.PROTOCOL_ERROR)
      }
      case _ => {
        context.parent ! Disconnect(ReasonCode.UNSPECIFIED_ERROR)
      }

    }

  }
}
