package io.evillair

import akka.actor.ActorSystem
import io.evillair.mqtt.actors.TCPServerActor

object Main extends App {

  implicit val system: ActorSystem = ActorSystem("valkyrie")

  val host: String = "localhost"
  val port: Int = 8080

  system.actorOf(TCPServerActor.props(host, port))

}
