name := "valkyrie"
organization := "io.evillair"
version := "0.1-SNAPSHOT"
scalaVersion := "2.12.8"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.19"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19"
